{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Main where

import           Data.Foldable       ( forM_ )
import           Data.Text.Encoding  ( decodeUtf8 )
import qualified Data.Text.IO        as TIO

import           Entry

import           Network.HTTP.Req    ( responseBody )

import           Options.Applicative

import           Params

import           Query

import           System.Exit         ( exitFailure )

import           TextShow            ( TextShow(showt) )

main :: IO ()
main = work =<< cmdLineParser

work :: Params -> IO ()
work Params{ .. } = do
    r <- makeRequest $ setQuery fromLang query
    case r of
        Nothing -> TIO.putStrLn "Could not complete request" >> exitFailure
        Just r' -> do
            let entries = scrapeTranslations . decodeUtf8 . responseBody $ r'
            forM_ entries printEntries
    return ()
  where
    printEntries [] =
        TIO.putStrLn $ mconcat [ "No results found for ", showt query ]
    printEntries es = forM_ es (TIO.putStrLn . renderEntry)
