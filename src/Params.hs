{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE StandaloneDeriving #-}

module Params ( Params(..), cmdLineParser ) where

import           Options.Applicative

import           Query

data Params = Params
    { query    :: Query
    , fromLang :: Lang
    }
    deriving ( Show )

deriving instance Show Lang

mkParams :: Parser Params
mkParams = Params <$> strArgument (metavar "WORD" <> help "Word to search")
    <*> option parseLang
               (metavar "LANG" <> long "language" <> short 'l'
                <> help "Source language")

cmdLineParser :: IO Params
cmdLineParser = execParser opts
  where
    opts = info (mkParams <**> helper)
                (fullDesc
                 <> progDesc "Search glosbe.com's en-vi or vi-en dictionaries")

parseLang :: ReadM Lang
parseLang = eitherReader $ \arg -> case arg of
    "en" -> Right En
    "vi" -> Right Vi
    _    -> Left $ mconcat [ "Invalid language: ", arg ]
