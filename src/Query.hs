{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeApplications #-}

module Query ( Lang(En, Vi), Query, setQuery, makeRequest ) where

import           Control.Monad.Error.Class ( MonadError(throwError) )
import           Control.Monad.Except
import           Control.Monad.IO.Class
import           Control.Monad.Trans.Maybe

import qualified Data.Text                 as T

import           Network.HTTP.Req

import           TextShow

data Lang = En | Vi
    deriving ( Enum, Bounded, Eq )

type Query = T.Text

instance TextShow Lang where
    showb En = "en"
    showb Vi = "vi"

instance (MonadIO m) => MonadHttp (MaybeT m) where
    handleHttpException _ = MaybeT $ return Nothing

instance (MonadIO m) => MonadHttp (ExceptT HttpException m) where
    handleHttpException = throwError

host :: T.Text
host = "glosbe.com"

setQuery :: Lang -> Query -> Url Https
setQuery from q = https host /: showt from /: showt (opposite from) /: q
  where
    opposite lang
        | lang == minBound = maxBound @Lang
        | otherwise = minBound @Lang

makeRequest :: Url a -> IO (Maybe BsResponse)
makeRequest url = runMaybeT $ req GET url NoReqBody bsResponse mempty
