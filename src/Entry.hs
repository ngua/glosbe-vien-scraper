{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Entry
    ( Definition
    , Meaning
    , PartOfSpeech(..)
    , Entry(..)
    , Example(..)
    , scrapeTranslations
    , renderEntry
    ) where

import           Control.Applicative

import           Data.ByteString               ( ByteString )
import           Data.Foldable                 ( find )
import           Data.List                     ( nub )
import           Data.Maybe                    ( catMaybes )
import qualified Data.Text                     as T

import           Prettyprinter
import           Prettyprinter.Render.Terminal

import           Query                         ( Query )

import           Text.HTML.Scalpel
import           Text.Regex.TDFA
import           Text.StringLike

import           TextShow

data Entry = Entry
    { definition   :: Definition
    , partOfSpeech :: [PartOfSpeech]
    , meaning      :: Maybe Meaning
    , number       :: Int
    , example      :: Maybe Example
    }
    deriving Show

data Example = Example
    { source :: T.Text
    , target :: T.Text
    }
    deriving Show

type Definition = T.Text

type Meaning = T.Text

type TextScraper = Scraper T.Text

data PartOfSpeech = Noun | Verb | Adj | Adv | Prep | Conj | Det | Pron
    deriving ( Show, Eq )

instance TextShow PartOfSpeech where
    showb Noun = "n."
    showb Verb = "v."
    showb Adj  = "adj."
    showb Adv  = "adv."
    showb Prep = "prep."
    showb Conj = "conj."
    showb Det  = "det."
    showb Pron = "pron."

posFromStr :: T.Text -> Maybe PartOfSpeech
posFromStr "pronoun" = Just Pron
posFromStr "adverb" = Just Adv
posFromStr s
    | s =~ ("conjunction[a-z]?" :: T.Text) = Just Conj
    | s =~ ("[a-z]?noun" :: T.Text) = Just Noun
    | s =~ ("[a-z]?verb" :: T.Text) = Just Verb
    | s =~ ("[ ad|prep ]position" :: T.Text) = Just Prep
posFromStr "adjective" = Just Adj
posFromStr "determiner" = Just Det
posFromStr "numeral" = Just Det
posFromStr _ = Nothing

scrapeTranslations :: T.Text -> Maybe [Entry]
scrapeTranslations bs =
    scrapeStringLike bs $ chroots ("li" @: [ hasClass "phraseMeaning" ]) $ do
        idx <- position
        d <- scrapeDefinition
        ps <- scrapePOS
        m <- tryScrapeMeaning <|> return mempty
        exSrc <- tryScrapeExampleSource <|> return mempty
        exTrgt <- tryScrapeExampleTarget <|> return mempty
        return $ Entry d
                       (textToPOS ps)
                       (if not . T.null $ m then Just m else Nothing)
                       (idx + 1)
                       (curry textToExample exSrc exTrgt)
  where
    scrapeDefinition :: TextScraper Definition
    scrapeDefinition = text $
        "div" @: [ hasClass "text-info" ] // "strong" @: [ hasClass "phr" ]
    --
    scrapePOS :: TextScraper T.Text
    scrapePOS = text $ "div" @: [ hasClass "text-info" ] // "div"
        @: [ hasClass "gender-n-phrase" ]
    --
    tryScrapeMeaning :: TextScraper Meaning
    tryScrapeMeaning = text $ "div" @: [ hasClass "meaningContainer" ] // "div"
        @: [ hasClass "phraseMeaning" ]
    --
    tryScrapeExampleSource :: TextScraper T.Text
    tryScrapeExampleSource =
        text $ "div" @: [ hasClass "examples" ] // "div" @: [ "dir" @= "ltr" ]
    --
    tryScrapeExampleTarget :: TextScraper T.Text
    tryScrapeExampleTarget = text $ "div" @: [ hasClass "examples" ] // "div"
        @: [ "lang" @=~ (makeRegex ("[en|vi]" :: T.Text) :: Regex) ]
    --
    textToExample :: (T.Text, T.Text) -> Maybe Example
    textToExample (s, t) =
        if any T.null [ s, t ] then Nothing else Just $ Example s t
    --
    textToPOS ps = nub . catMaybes $ posFromStr <$> T.splitOn "," (stripped ps)
    --
    stripped = T.strip . T.filter (`notElem` [ '{', '}', ' ' ])

render :: Doc AnsiStyle -> T.Text
render = renderStrict . layoutPretty defaultLayoutOptions

renderEntry :: Entry -> T.Text
renderEntry Entry{ .. } = render $
    vsep [ annNumber <+> dot <+> annDefinition <+> annPos
         , annMeaning
         , annExamples
         , pretty ("\n" :: T.Text)
         ]
  where
    annDefinition = annotate bold . pretty $ definition
    --
    annNumber     = pretty $ number
    --
    annMeaning    = case meaning of
        Nothing -> mempty
        Just m  -> annotate italicized $ pretty m
    --
    annPos        = case partOfSpeech of
        [] -> mempty
        ps -> indent 10 . tupled $
            foldr (\a b ->
                   (annotate (color Blue <> italicized) . pretty $ showt a) : b)
                  mempty
                  ps
    --
    annExamples   = case example of
        Nothing -> mempty
        Just Example{ .. } ->
            hsep [ pretty source
                 , space <+> pipe <+> space
                 , annotate (colorDull Green) . pretty $ target
                 ]
